#!/usr/bin/env python
# -*- coding:utf-8 -*-

# conf_bot должен содержать
# api_token формата '240**8***:A*E****r-l*****Z56dJJN*****S5L***I',
# bot_module - Бот модуль. Модуль должен содержать методы
#           runCommand("команда") -  для запуска команды
#           init_notification_bot(бот) - Если будет использоваться бот для уведомлений
# DEBUG_M - если требуется вывод в консоль
# timer_for_reconnect - период для попыток переподклчения, при обрыве соединения
#           по умолчанию значение для тайминга 2.0

import conf_bot
import telebot
import exceptions
import requests

import threading

try:
    from my_dict_comm import bot_answers
except ImportError:
    from dict_comm import bot_answers

try:
    from my_dict_comm import bot_help_for_commands
except ImportError:
    from dict_comm import bot_help_for_commands

# from conf_bot import bot_module

bot_module = None

def init_bot_module(bm):
    global bot_module
    bot_module = bm

import sys,signal
from time import sleep

default_timing = 2.0

# Для вывода в консоль
hasDebug = hasattr(conf_bot,"DEBUG_M")
if hasDebug == True and conf_bot.DEBUG_M == True:
    isDebug = True
else:
    isDebug = False

# Для подключения уведомлений от бота
hasNotif = hasattr(conf_bot,"isNotifBot")
if hasNotif == True and conf_bot.isNotifBot == True:
    isNotification_bot = True
else:
    isNotification_bot = False

# Для завершения процесса при вылете не перехваченных исключений
# из основной программы
killProc = False

class tangoBot(object):
    def __init__(self):
        if isNotification_bot is True:
            if isDebug:
                print("init pyTelegramBot with init_notification bot")
            self.isNotifBot = True
        else:
            if isDebug:
                print("init pyTelegramBot")
            self.isNotifBot = False

    def readingMes(self, mess):
        text = mess.text

        fromSplit = self.splitCommands(text)
        comm = self.checkDictComm(fromSplit[0])

        # проверка типа возвращаемого словарём
        # Это может быть str - если значение ключа есть строка
        # Или вложенный словарь
        # Если данного слова нет в словаре, возращается None
        # И слово передаётся в модуль bot_module в виде list
        if type(comm) is dict:
            if len(fromSplit) == 1:
                # Для вывода help для команды, которая должна содержать аргументы,
                # желательно определить ключ [''], для вывода дополнительной информации
                hasKey = comm['']
                if hasKey is None:
                    return "Check command"
                return hasKey

            if len(fromSplit) == 2:
                if fromSplit[1] not in comm:
                    if "noInformation" in bot_answers:
                        return bot_answers.get("noInformation")
                    else:
                        return "No information for this argument"
                return comm.get(fromSplit[1])

            if len(fromSplit) > 2:
                if "moreThanOne" in bot_answers:
                    return bot_answers.get("moreThanOne")
                else:
                    return "request commands_help must not contain more than one argument"

        elif type(comm) is str:
            return comm

        elif comm is None:
            # Если команды нет в bot_help_for_commands, производится попытка
            # запуска команды из включённого модуля. Включённый модуль также
            # может содержать помощь по командам
            answFromBotModule = bot_module.runCommand(fromSplit)
            if answFromBotModule is None:
                hasKey = bot_answers.get("None")
                if hasKey is None:
                    return "No information for this command"
                return hasKey
            else:
                return answFromBotModule

    def splitCommands(self, command):
        return command.split(" ")


    def checkDictComm(self,command):
        if command in bot_help_for_commands:
            answ = bot_help_for_commands[command]
        else:
            answ = None
        return answ


    # Добавление пользователя в список рассылки
    def addUserToList(self,message):
        # self.isNotifBot может быть установлен в false, если в
        # подключаемом модуле не определён метод init_notification_bot
        if self.isNotifBot is not True:
            if "notSuppNotif" in bot_answers:
                answ = bot_answers.get("notSuppNotif")
            else:
                answ = "Bot does not support notifications"
            bot.send_message(message.chat.id, answ)
            return

        user = message.from_user
        if isDebug:
            print("User {0} tries to add itself to list".format(str(user.username)))
        try:
            answFromBotModule = bot_module.addUser(user)
            if answFromBotModule is not None:
                bot.send_message(message.chat.id, answFromBotModule)
            else:
                # ??? Add answer
                if "notAnswer" in bot_answers:
                    bot.send_message(message.chat.id, bot_answers['notAnswer'])
                else:
                    bot.send_message(message.chat.id, "Not answer from module")
        except exceptions.AttributeError as err:
            if isDebug:
                print(err.message)
            if "noMethod" in bot_answers:
                bot.send_message(message.chat.id, bot_answers.get("noMethod") )
            else:
                bot.send_message(message.chat.id, err.message)
        except:
            errmes = "Unknown exception from addUserToList"
            if isDebug:
                print(errmes)
            bot.send_message(message.chat.id, errmes)

    # Удаление пользователя из списка рассылки
    def removeUserFromList(self,message):
        # self.isNotifBot может быть установлен в false, если в
        # подключаемом модуле не определён метод init_notification_bot
        if self.isNotifBot is not True:
            if "notSuppNotif" in bot_answers:
                answ = bot_answers.get("notSuppNotif")
            else:
                answ = "Bot does not support notifications"
            bot.send_message(message.chat.id, answ)
            return

        user = message.from_user
        if isDebug:
            print("User {0} tries to remove itself from list".format(str(user.username)))
        try:
            answFromBotModule = bot_module.removeUser(user)
            if answFromBotModule is not None:
                bot.send_message(message.chat.id, answFromBotModule)
        except exceptions.AttributeError as err:
            if isDebug:
                print(err.message)
            if "noMethod" in bot_answers:
                bot.send_message(message.chat.id, bot_answers.get("noMethod") )
            else:
                bot.send_message(message.chat.id, err.message)
        except:
            errmes = "Unknown exception from removeUserFromList"
            if isDebug:
                print(errmes)
            bot.send_message(message.chat.id,errmes)



# Проверка используется ли бот для уведомлений
isNotificationBot = hasattr(conf_bot,"isNotifBot")
if isNotificationBot is True and conf_bot.isNotifBot is True:
    isNotifBot = True
else:
    isNotifBot = False

# Инициализация бота
api_token = conf_bot.api_token
bot = telebot.TeleBot(api_token)
tb = tangoBot()

# Вызов метода для добавления пользователя в список рассылки
# Подключённый модуль bot_module для бота должен содержать метод
# addUser(user) для добавления пользователя
@bot.message_handler(commands=['addme'])
def add_to_notif(message):
    if isNotifBot is True:
        tb.addUserToList(message)
    else:
        if "notSuppNotif" in bot_answers:
            answ = bot_answers.get("notSuppNotif")
        else:
            answ = "Bot does not support notifications"
        bot.send_message(message.chat.id, answ)

# Вызов метода для удаления пользователя из списка рассылки
# Подключённый модуль для бота должен содержать метод
# removeUser(user) для удаления пользователя
@bot.message_handler(commands=['removeme'])
def remove_from_notif(message):
    if isNotifBot is True:
        tb.removeUserFromList(message)
    else:
        if "notSuppNotif" in bot_answers:
            answ = bot_answers.get("notSuppNotif")
        else:
            answ = "Bot does not support notifications"
        bot.send_message(message.chat.id, answ)

# Обработчик сообщений для бота
@bot.message_handler(func=lambda message: True, content_types=['text'])
def echo_msg(message):
    out = tb.readingMes(message)
    bot.send_message(message.chat.id, out)

# Для таймера
hasTiming = hasattr(conf_bot,"timer_for_reconnect")
if hasTiming == True:
    isDig = isinstance(conf_bot.timer_for_reconnect,(int, long, float))
    if isDig is True and conf_bot.timer_for_reconnect >= 1.0:
        timing = conf_bot.timer_for_reconnect
    else:
        timing = default_timing
else:
    timing = default_timing

def printExc(exc):
    print(type(exc))  # the exception instance
    print(exc.args)  # arguments stored in .args
    print(exc)
    print("Exception from bot polling")

def botPolling():
    try:
        if isDebug:
            print("Bot polling start")
        bot.polling(none_stop=True)
    except requests.exceptions.ReadTimeout as exc:
        printExc(exc)
        threading.Timer(timing, botPolling).start()
    except requests.exceptions.ConnectionError  as exc:
        printExc(exc)
        threading.Timer(timing, botPolling).start()
    except BaseException as exc:
        # Пока не получается нормально возобновить bot.polling
        # поэтому процесс просто убивается.
        # Для восстановления можно прописать скрипт в crontab
        printExc(exc)
        threading.Timer(timing, botPolling).cancel()
        global killProc
        killProc = True


def exitfRun(isExc=False):
    # Выход из программы. Либо после CTRL+C, либо при выбросе
    # необработанного исключения из основной программы
    global thread
    thread.alive = False
    if isNotifBot is True and bot_module is not None:
        hasStopNotificationThread = hasattr(bot_module, "stopNotificationThread")
        # В bot_module должен быть определён метод stopNotificationThread()
        # В этом методе, к примеру, нужно останавливать таймер для методов
        if hasStopNotificationThread is True:
            bot_module.stopNotificationThread()
    if isExc == True:
        sys.exit(1)
    else:
        sys.exit(0)

def handler(signal, frame):
    print "Ctrl-C.... Exiting"
    exitfRun()

# Для перевода потока в режим демона и возможности завершения через SIGINT
hasThreadDeamon = hasattr(conf_bot,"thread_daemon")
if hasThreadDeamon == True and conf_bot.thread_daemon == True:
    thread_daemon = True
else:
    thread_daemon = False

def forMain():
    global thread
    if thread_daemon is True:
        signal.signal(signal.SIGINT, handler)
    thread = threading.Thread(target=botPolling)

    if thread_daemon is True:
        thread.daemon = True
    thread.start()

    while True:
        sleep(1)
        global killProc
        if killProc is True:
            print("Kill proc")
            exitfRun(killProc)

    # if thread_daemon is True:
    #     while True:
    #         sleep(1)
